import time
import RPi.GPIO as GPIO

isOn = 0
timeStart = 0
downtime = 30
sensorPin = 16
interruptEnable = 1


# Check if 30s elapsed from current value of timeStart
def timer():
	global isOn
	global timeStart
	
	# print("Time is {0}, sleeping until {1}".format(time.time(), timeStart+downtime))
	
	# Sleep until 30 seconds from timeStart
	time.sleep(timeStart + downtime - time.time())
	if time.time()-timeStart >= downtime and isOn == 1:
		isOn = 0
	else:
		timer()


# Interrupt handler
def msInterruptHandler(channel):
	global isOn
	global timeStart

	# print("Motion detected ->> Interrupt!")
	timeStart = time.time()
	isOn = 1
	timer()


# Initializer
def motionSensorInterrupts():
	global sensorPin

	# Delay for PIR initialization
	time.sleep(45)
	# Pin setup
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(sensorPin, GPIO.IN)
	# Setup interrupt
	GPIO.add_event_detect(sensorPin, GPIO.RISING, callback=msInterruptHandler, bouncetime=500)


# # Interrupt handler
# def msInterruptHandler(channel):
# 	global isOn
# 	global timeStart
# 	global downtime
# 	global sensorPin
# 	global interruptEnable

# 	if interruptEnable == 1:
# 		print("Motion detected ->> Interrupt!")
# 		timeStart = time.time()
# 		isOn = 1
# 		interruptEnable = 0

# 		# Polling the sensorPin for PIR activity
# 		while 1:
# 			if GPIO.input(sensorPin):
# 				timeStart = time.time()

# 			if time.time()-timeStart >= downtime and isOn == 1:
# 				isOn = 0
# 				break

# 		print("Times up, interrupt reenabled.")
# 		interruptEnable = 1

	
# def motionSensor():
# 	global isOn
# 	global timeStart
# 	global downtime
# 	global sensorPin
	
# 	# Delay for PIR initialization
# 	time.sleep(45)
# 	# Pin setup
# 	GPIO.setmode(GPIO.BCM)
# 	GPIO.setup(sensorPin, GPIO.IN)

# 	# Countdown timer for PIR inactivity
# 	timeStart = time.time()
# 	# Polling the sensorPin for PIR activity
# 	while 1:
# 		if GPIO.input(sensorPin):
# 			timeStart = time.time()
# 			isOn = 1

# 		if time.time()-timeStart >= downtime and isOn == 1:
# 			isOn = 0


