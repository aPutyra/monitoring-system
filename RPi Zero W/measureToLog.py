#!/usr/bin/python3
import os
import time
import timestamp

def measure_temp():
		temp = os.popen("vcgencmd measure_temp").readline()
		temp = temp[:-3]
		return (temp.replace("temp=",""))


def measure_cpu(pid):
		cpu = os.popen("top -p {0} -b -d1 -n1|grep -i \"{1}\"|head -c51|cut -c 48-51".format(pid, pid)).readline()
		cpu = cpu[:-1]
		return (cpu.replace("cpu=",""))

while True:
		tempString = "echo \"{0}, temp= {1}\" >> TempLog.txt".format(timestamp.timeStamp(), measure_temp())
		cpuString = "echo \"{0}, cpu= {1}\" >> CpuLog.txt".format(timestamp.timeStamp(), measure_cpu(3553))
		os.popen(tempString).readline()
		os.popen(cpuString).readline()
		time.sleep(60)

