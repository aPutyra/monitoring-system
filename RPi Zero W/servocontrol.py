import pigpio
servoLRpin = 12
servoUDpin = 13
stateLR = 18
stateUD = 18

#Initializing the servos
try:
	pi = pigpio.pi()
except:
	print("pigpio error: ", sys.exc_info()[0])
	raise


pi.set_PWM_frequency(servoLRpin, 50)
pi.set_PWM_dutycycle(servoLRpin, stateLR)
pi.set_PWM_frequency(servoUDpin, 50)
pi.set_PWM_dutycycle(servoUDpin, stateUD)


#Function which handles servo control
def servoControl ( direction ):
	global stateLR
	global stateUD
	global pwmLR
	global pwmUD

	if direction == "UP":
		stateUD = stateUD - 1
		pi.set_PWM_dutycycle(servoUDpin, stateUD)

	elif direction == "DOWN":
		stateUD = stateUD + 1
		pi.set_PWM_dutycycle(servoUDpin, stateUD)

	elif direction == "LEFT":
		stateLR = stateLR + 1
		pi.set_PWM_dutycycle(servoLRpin, stateLR)

	elif direction == "RIGHT":
		stateLR = stateLR - 1
		pi.set_PWM_dutycycle(servoLRpin, stateLR)

	else:
		print("Invalid command to servoControl: {0}".format(direction))
	