import RPi.GPIO as GPIO

GPIO.setwarnings(False)

ledPin1 = 20
ledPin2 = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(ledPin1, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(ledPin2, GPIO.OUT, initial=GPIO.LOW)

##Flash function
def ledFlash(luminance):
	if (luminance<40.0):
		GPIO.output(ledPin1, GPIO.HIGH)
		GPIO.output(ledPin2, GPIO.HIGH)
	else:
		GPIO.output(ledPin1, GPIO.LOW)
		GPIO.output(ledPin2, GPIO.LOW)

def ledOn():
	GPIO.output(ledPin1, GPIO.HIGH)
	GPIO.output(ledPin2, GPIO.HIGH)
