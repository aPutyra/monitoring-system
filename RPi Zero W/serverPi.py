#!/usr/bin/python3

##### System libs/modules #####
import socket
import time
import picamera
import subprocess
import datetime as dt
import RPi.GPIO as GPIO
import threading
import sys
import os

subprocess.call(["sudo", "killall", "pigpiod"])
subprocess.call(["sudo", "pigpiod"])

##### Personal libs #####
import motionsensor
import sockethandler
import ledflash
import servocontrol
from timestamp import timeStamp
from calclum import calcLum

##### Variables #####
ledOnUser = 0
cameraOnUser = 0
cameraOnMotion = 0
recordingsDirectory = "recordings/"
current_timestamp = ""

##### Handler functions for incoming messages from Android app #####
def cameraToggle():
	global cameraOnUser

	if cameraOnUser == 0:
		##### Setup vlc media server #####
		cmdline = ['cvlc', '-q', 'stream:///dev/stdin', '--sout', "#standard{access=http,mux=ts,dst=:32898}", ':demux=h264' ]
		#cmdline = ["cvlc", "-vvv", "stream:///dev/stdin", "--sout", "#transcode{fps=25,vcodec=VP80,acodec=none,ab=56,samplerate=44100,scodec=none}:http{mux=webm,dst=:8080/}", ":sout-all", ":sout-keep", ":demux=h264" ]
		player = subprocess.Popen(cmdline, stdin=subprocess.PIPE)
		camera.start_recording(player.stdin, format='h264', splitter_port=1)
		cameraOnUser = 1
	else:
		camera.stop_recording(splitter_port=1)
		cameraOnUser = 0

def flashToggle():
	global ledOnUser

	if ledOnUser == 0:
		ledflash.ledOn()
		ledOnUser = 1
	else:
		ledOnUser = 0

def deleteRec():
	recordings = os.listdir(recordingsDirectory)
	for item in recordings:
		if item.endswith(".h264"):
			os.remove(os.path.join(recordingsDirectory, item))

def arrowUp():
	servocontrol.servoControl("UP")

def arrowDown():
	servocontrol.servoControl("DOWN")

def arrowLeft():
	servocontrol.servoControl("LEFT")

def arrowRight():
	servocontrol.servoControl("RIGHT")

def default():
	print("Invalid entry.")

messageSwitcher = {
	"cameraToggle": cameraToggle,
	"flashToggle": flashToggle,
	"deleteRec": deleteRec,
	"arrowUp": arrowUp,
	"arrowDown": arrowDown,
	"arrowLeft": arrowLeft,
	"arrowRight": arrowRight
}

##### Creating a thread to check motion sensor output #####
motionThread = threading.Thread(target = motionsensor.motionSensorInterrupts, args=())
motionThread.daemon = True
motionThread.start()

##### Creating a thread to handle socket messages from/to Android app #####
socketThread = threading.Thread(target = sockethandler.messageHandler, args=())
socketThread.daemon = True
socketThread.start()

##### Initialize the camera #####
camera = picamera.PiCamera()
camera.resolution = (640, 480)
camera.framerate = 30

##### Video monitoring system main process #####
while True:

	# Sleep for 250ms
	time.sleep(.250)

	if cameraOnUser or cameraOnMotion:
		# Check for camera exceptions
		try:
			if cameraOnUser:
				camera.wait_recording(timeout=0, splitter_port=1)
			if cameraOnMotion:
				camera.wait_recording(timeout=0, splitter_port=2)
		except:
			print("Unexpected error: ", sys.exc_info()[0])
			raise
    	
    	# Add timestamp to video stream
		if current_timestamp != dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S'):
			current_timestamp = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
			camera.annotate_text = current_timestamp

    	# Every 10 seconds check luminance and turn on flash if it is dark & if user hasn't toggled it on via app
		if dt.datetime.now().second%10 == 0 and ledOnUser == 0:
			camera.capture('video_still.jpg', use_video_port=True)
			luminance = calcLum()
			ledflash.ledFlash(luminance)

	##### Motion sensor triggered recording #####
	if motionsensor.isOn == 1 and cameraOnMotion == 0:
		motionName = 'recordings/' + timeStamp() + '.h264'
		camera.start_recording(motionName, format='h264', splitter_port=2)
		print("Recording to %s" % motionName)
		cameraOnMotion = 1
	if motionsensor.isOn == 0 and cameraOnMotion == 1:
		camera.stop_recording(splitter_port=2)
		print("Finished recording")
		cameraOnMotion = 0

	##### Socket message handling #####
	if sockethandler.dataFlag == 1:
		func = messageSwitcher.get(sockethandler.dataBuffer[:-1], default)
		func()
		sockethandler.dataFlag = 0


