import socket

def messageHandler() :
	global dataFlag
	global dataBuffer
	dataFlag = 0

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(("192.168.0.12", 32896))
	s.listen(1)

	while 1:
		if dataFlag == 0:
			print ("Listening for a connection.")
			conn, addr = s.accept()
			data = conn.recv(1024)
			dataBuffer = data.decode('utf-8')
			print(dataBuffer[:-1])
			if not dataBuffer:
				break
			conn.send(data)
			conn.close()
			dataFlag = 1
