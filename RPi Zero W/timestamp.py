import datetime as dt

def timeStamp ():
	ts = dt.datetime.today()
	ret_ts = ("{0}_{1}_{2}-{3}h{4}m{5}s".format(ts.day, ts.month, ts.year, ts.hour, ts.minute, ts.second))
	return ret_ts