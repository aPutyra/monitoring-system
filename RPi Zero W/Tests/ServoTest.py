import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(12, GPIO.OUT)
pwm = GPIO.PWM(12, 50)
pwm.start(7.5)
time.sleep(5)

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step

for x in my_range(5, 10, 0.02):
    pwm.ChangeDutyCycle(x)
    time.sleep(0.09)

pwm.ChangeDutyCycle(7.5)
time.sleep(5)
pwm.stop()
GPIO.cleanup()