import RPi.GPIO as GPIO
import time
import socket
import struct
import sys

# GPIO.setmode(GPIO.BCM)
# GPIO.setup(12, GPIO.OUT)
# pwm = GPIO.PWM(12, 50)
# pwm.start(7.5)
# time.sleep(5)

# pwm.ChangeDutyCycle(7.5)
# time.sleep(5)
# pwm.stop()
# GPIO.cleanup()

#main function
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('localhost', 50000))
s.listen(1)
conn, addr = s.accept()
while 1:
    data = conn.recv(1024)
    print(data)
    if not data:
        break
    conn.sendall(data)
conn.close()