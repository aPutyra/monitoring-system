import time
import RPi.GPIO as GPIO
import threading as thread

sensorPin = 16
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensorPin, GPIO.IN)

timeStart = time.time()
isOn = 0

def motionCamera():
	while 1:
		if GPIO.input(sensorPin):
			print("Turn on/renew")
			timeStart = time.time()
			isOn = 1

		if time.time()-timeStart >= 30 and isOn == 1:
			isOn = 0
			print("Turn off")
			break

GPIO.cleanup()