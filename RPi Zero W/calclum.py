from PIL import Image
import os

def calcLum():
	still = Image.open('video_still.jpg').convert('L')
	os.remove("video_still.jpg")
	width, height = still.size
	pixel_values = list(still.getdata())
	retVal = sum(pixel_values)/float(len(pixel_values))
	#print("Luminance: %f" % retVal)
	return retVal