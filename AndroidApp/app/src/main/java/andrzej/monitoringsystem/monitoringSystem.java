package andrzej.monitoringsystem;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class monitoringSystem extends AppCompatActivity {

    VideoView videoView;
    EditText cameraIP;
    String socketURL = "";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring_system);

        videoView = (VideoView)findViewById(R.id.videoView);
        cameraIP = (EditText)findViewById(R.id.cameraIP);

        context = getApplicationContext();
    }

    public void sendMessage (final String outputMessage) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                	//Setup socket & use it as an output
                    socketURL = cameraIP.getText().toString();
                    Socket s = new Socket(socketURL, 32896);
                    s.setSoTimeout(5000);
                    s.setSoLinger( false, 5000);
                    OutputStream outputStream = s.getOutputStream();
                    PrintWriter output = new PrintWriter(outputStream);

                    //Send outputMessage
                    output.println(outputMessage);
                    output.flush();

                    //Read server response
                    BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    final String serverReply = input.readLine();
                    //Handle server response
                    if (serverReply.equals(outputMessage)) {
                        monitoringSystem.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(context, "Message " + outputMessage + " successfully sent.", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        });
                    } else {
                        monitoringSystem.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(context, "Server reply is invalid: " + serverReply, Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        });
                    }

                    //Closing outputs
                    output.close();
                    outputStream.close();
                    s.close();
                    //Handling exceptions
                    } catch (UnknownHostException e) {
                        System.out.println("UnknownHostException");
                        System.out.println(e);
                    } catch (SocketTimeoutException e) {
                    monitoringSystem.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(context, "Socket timeout", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
                    } catch (IOException e) {
                        System.out.println("IOException - trying to connect to server");
                        System.out.println(e);
                    }
            }
        });

        //Run the thread defined above
        thread.start();
    }


    public void renewConnection (View v) {
//        Toast toast = Toast.makeText(context, "renewConnection", Toast.LENGTH_SHORT);
//        toast.show();

    	//Define socket
        socketURL = cameraIP.getText().toString();
        String videoURL = "http://" + socketURL + ":32897";
        //Setup media player to videoURL
        try{
            Uri uri = Uri.parse(videoURL);
            videoView.setVideoURI(uri);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    Toast toast = Toast.makeText(context, "on Completion", Toast.LENGTH_SHORT);
//                    toast.show();
                }
            });
        }
        catch(Exception exc){
            Log.e("Error", exc.getMessage());
            exc.printStackTrace();
        }
        finally {
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    String error = "What = " + what + ", Extra = " + extra;
                    Toast toast = Toast.makeText(context, error, Toast.LENGTH_LONG);
                    toast.show();
                    return true;
                }
            });
        }
        //Play the video stream
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Toast toast = Toast.makeText(context, "on Prepared", Toast.LENGTH_SHORT);
                toast.show();
                mp.setLooping(true);
                videoView.start();
            }
        });
    }

    //Simple onClick handlers for buttons which send an appropriate message

    public void cameraToggle (View v) {
        sendMessage("cameraToggle");
    }

    public void flashToggle (View v) {
        sendMessage("flashToggle");
    }

    public void deleteRec (View v) {
        sendMessage("deleteRec");
    }

    public void arrowUp(View v) {
        sendMessage("arrowUp");
    }

    public void arrowDown (View v) {
        sendMessage("arrowDown");
    }

    public void arrowLeft (View v) {
        sendMessage("arrowLeft");
    }

    public void arrowRight (View v) {
        sendMessage("arrowRight");
    }

}
